import {
  _decorator,
  Component,
  Button,
  director,
  game,
  find,
  instantiate,
  Director,
} from "cc";
import { BackgroundSoundtrack } from "../audio/backgroundSoundtrack";
import { BUTTON_EVENT } from "../enum/button";
import { SCENE_KEY } from "../enum/sceneName";
import { TRANSITION_SCREEN_EVENT } from "../enum/transitionScreen";
import { BaseButton } from "../object/baseButton";
import { TransitionScreen } from "../sprite/transitionScreen";
const { ccclass, property } = _decorator;

@ccclass("TitleScene")
export class TitleScene extends Component {
  private readonly backgroundSoundtrackPersistNodeName =
    "BACKGROUND_SOUNDTRACK_PERSIST";

  @property(Button)
  public readonly playButton?: BaseButton | null;

  @property(BackgroundSoundtrack)
  public readonly backgroundSoundtrack?: BackgroundSoundtrack | null;

  @property(TransitionScreen)
  public readonly transitionScreen?: TransitionScreen | null;

  start() {
    this.spawnBackgroundSoundtrackIfNotExist();
    this.transitionScreen?.fadeOut(0.5);
    this.transitionScreen?.node.once(
      TRANSITION_SCREEN_EVENT.FADE_OUT_COMPLETE,
      () => {
        this.setupPlayButtonClick();
      }
    );
  }

  private spawnBackgroundSoundtrackIfNotExist() {
    const { backgroundSoundtrack, backgroundSoundtrackPersistNodeName } = this;

    if (!backgroundSoundtrack) return;

    if (!find(backgroundSoundtrackPersistNodeName)) {
      const node = instantiate(backgroundSoundtrack.node);
      node.name = backgroundSoundtrackPersistNodeName;
      node.getComponent(BackgroundSoundtrack)?.play();
      director.on(Director.EVENT_AFTER_SCENE_LAUNCH, () => {
        node.getComponent(BackgroundSoundtrack)?.play();
      });
      game.addPersistRootNode(node);
    }
  }

  private setupPlayButtonClick() {
    this.playButton?.node.on(BUTTON_EVENT.TOUCH_END, () => {
      this.playButton?.unregisterTouchEvent();
      this.goToGameScene();
    });
  }

  private goToGameScene() {
    this.transitionScreen?.fadeIn(0.5);
    this.transitionScreen?.node.once(
      TRANSITION_SCREEN_EVENT.FADE_IN_COMPLETE,
      () => {
        director.loadScene(SCENE_KEY.GAME_SCENE)
      }
    );
  }
}
