import { _decorator, Component, Node, director } from "cc";
import { SilenceSFX } from "../audio/noneSFX";
import { PreloadControl } from "../control/preloadControl";
import { PRELOAD_CONTROL_EVENT } from "../enum/preloadControl";
import { SCENE_KEY } from "../enum/sceneName";
import { ASSET_LOADER_EVENT } from "../lib/enum/assetLoader";
import { AssetLoader } from "../lib/loader/assetLoader";
import { AssetLoaderUI } from "../lib/loader/assetLoaderUI";
const { ccclass, property } = _decorator;

@ccclass("LoadingScene")
export class LoadingScene extends Component {
  @property(AssetLoader)
  public readonly assetLoader?: AssetLoader | null;

  @property(AssetLoaderUI)
  public readonly assetLoaderUI?: AssetLoaderUI | null;

  @property(SilenceSFX)
  public readonly silenceSFX?: SilenceSFX | null;

  @property(PreloadControl)
  public readonly preloadControl: PreloadControl | null;

  start() {
    // start load asset
    this.startAssetsLoad();
  }

  private startAssetsLoad() {
    const { assetLoader, assetLoaderUI } = this;

    if (!assetLoader || !assetLoaderUI) {
      return;
    }

    assetLoader.node.on(ASSET_LOADER_EVENT.START, (progress: number) => {
      assetLoaderUI.updateText(progress);
    });

    assetLoader.node.on(
      ASSET_LOADER_EVENT.ASSET_LOAD_SUCCESS,
      (progress: number, key: string) => {
        assetLoaderUI.updateText(progress, key);
      }
    );

    assetLoader.node.on(ASSET_LOADER_EVENT.COMPLETE, (progress: number) => {
      assetLoaderUI.updateText(progress);
      this.onComplete();
    });

    assetLoader.startAssetsLoad();
  }

  private onComplete() {
    this.silenceSFX?.play(); // This is here to bypass audio autoplay on web

    this.preloadControl?.registerTouchEvent();
    this.preloadControl?.node.once(PRELOAD_CONTROL_EVENT.TOUCH_END, () => {
      this.goToTitleScene();
    });
  }

  private goToTitleScene() {
    director.loadScene(SCENE_KEY.LANDING_SCENE)
  }
}
