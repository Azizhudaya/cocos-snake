export enum GAME_OVER_EVENT {
  CANCEL = "cancel",
  PLAY_AGAIN = "play_again",
}
