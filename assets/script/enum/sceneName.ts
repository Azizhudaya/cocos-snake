export enum SCENE_KEY {
  LOADIMG_SCENE = 'loadingScene',
  LANDING_SCENE = 'landingScene',
  GAME_SCENE = 'gameScene' 
}