export enum SNAKE_BODY_PART {
  HEAD = "head",
  BODY = "body",
  TAIL = "tail",
  BODY_FAT = "body_fat",
  BODY_M0VE = "body_move",
  BODY_FAT_MOVE = "body_fat_move",
}

export enum SNAKE_EVENT {
  MOVE = "move",
}

export enum SNAKE_BODY_FRAME {
  HEAD = 0,
  BODY = 1,
  TAIL = 3,
  BODY_FAT = 2,
  BODY_M0VE = 4,
  BODY_FAT_MOVE = 5,
}
