import { math } from "cc";
import { SnakeSprite } from "../sprite/snakeSprite";


export interface SnakePart {
  sprite: SnakeSprite;
  index: math.Vec2;
  position: math.Vec3;
  rotation: math.Vec3;
  direction?: math.Vec2;
}

export interface SnakeConfig {
  parts: Array<{ x: number, y: number }>;
  interval: SnakeUpdateIntervalConfig;
}

export interface SnakeUpdateIntervalConfig {
  initial: number;
  accelerateMultiplier: number;
  accelerateEvery: number;
  minimum: number;
}