import { _decorator, Component, Node } from "cc";
import { IntroStageHeader } from "./introStageHeader";
import { PlayStageHeader } from "./playStageHeader";
const { ccclass, property } = _decorator;

@ccclass("GameHeader")
export class GameHeader extends Component {
  @property(PlayStageHeader)
  public readonly playStageHeader?: PlayStageHeader;

  @property(IntroStageHeader)
  public readonly introStageHeader?: IntroStageHeader;

  start() {
    this.introStageHeader?.show();
    this.playStageHeader?.hide();
  }

  public showIntro() {
    this.introStageHeader?.show();
  }

  public hideIntro() {
    this.introStageHeader?.hide();
  }

  public showScore() {
    this.playStageHeader?.show();
  }

  public hideScore() {
    this.playStageHeader?.hide();
  }

  public updateScore(score: number) {
    this.playStageHeader?.updateScore(score);
  }

  public updateHighScore(score: number) {
    this.playStageHeader?.updateHighScore(score);
  }
}
