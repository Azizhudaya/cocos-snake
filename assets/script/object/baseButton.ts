import { Button, _decorator, Node } from "cc";
import { BUTTON_EVENT } from "../enum/button";
const { ccclass } = _decorator;

@ccclass("BaseButton")
export class BaseButton extends Button {
  start() {
    this.registerTouchEvent();
  }
  private registerTouchEvent() {
    this.node.on(Node.EventType.TOUCH_END, () => {
      this.node.emit(BUTTON_EVENT.TOUCH_END);
    });
  }
  public unregisterTouchEvent() {
    this.node.off(Node.EventType.TOUCH_END);
  }
}
