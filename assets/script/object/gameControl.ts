import { _decorator, Component, Node, input, Input, KeyCode, math, v2 } from "cc";
import { CrashSFX } from "../audio/crashSFX";
import { EatSFX } from "../audio/eatSFX";
import { TurnSFX } from "../audio/turnSFX";
import { GAME_CONTROL_EVENT } from "../enum/gameControl";
import { KEYPAD_EVENT } from "../enum/keypad";
import { SNAKE_EVENT } from "../enum/snake";
import { GameBoard } from "./gameBoard";
import { KeypadScript } from "./keypadScript";
import { Snake } from "./snake";
const { ccclass, property } = _decorator;

@ccclass("GameControl")
export class GameControl extends Component {
  @property(GameBoard)
  public readonly gameBoard?: GameBoard;

  @property(Snake)
  public readonly snake?: Snake;

  @property(KeypadScript)
  public readonly keypad?: KeypadScript;

  @property(CrashSFX)
  public readonly crashSFX: CrashSFX;

  @property(EatSFX)
  public readonly eatSFX: EatSFX;

  @property(TurnSFX)
  public readonly turnSFX: TurnSFX;

  private isAlive = true;

  start() {
    this.setupControl();
  }

  public startGame() {
    this.setupSnakeMoveListener();
    this.snake?.move(); // Move immediately at the beginning without waiting for update interval
    this.snake?.startMoving();
  }

  private setupControl() {
    const { keypad } = this;

    if (!keypad) return;

    keypad.node.on(KEYPAD_EVENT.PRESS_UP, () => {
      this.changeSnakeDirection(0, -1);
    });

    keypad.node.on(KEYPAD_EVENT.PRESS_RIGHT, () => {
      this.changeSnakeDirection(1, 0);
    });

    keypad.node.on(KEYPAD_EVENT.PRESS_DOWN, () => {
      this.changeSnakeDirection(0, 1);
    });

    keypad.node.on(KEYPAD_EVENT.PRESS_LEFT, () => {
      this.changeSnakeDirection(-1, 0);
    });

    this.node.once(Node.EventType.NODE_DESTROYED, () => {
      input.off(Input.EventType.KEY_DOWN);
    });

    input.on(Input.EventType.KEY_DOWN, (event) => {
      switch (event.keyCode) {
        case KeyCode.ARROW_UP: {
          this.changeSnakeDirection(0, -1);
          break;
        }
        case KeyCode.ARROW_RIGHT: {
          this.changeSnakeDirection(1, 0);
          break;
        }
        case KeyCode.ARROW_DOWN: {
          this.changeSnakeDirection(0, 1);
          break;
        }
        case KeyCode.ARROW_LEFT: {
          this.changeSnakeDirection(-1, 0);
        }
        default: {
          break;
        }
      }
    });
  }

  private changeSnakeDirection(x: number, y: number) {
    const isDirectionChanged = this.snake?.changeDirection(x, y);
    if (isDirectionChanged) {
      this.snake?.node.once(SNAKE_EVENT.MOVE, () => {
        this.turnSFX?.play();
      });
    }

    this.node.emit(GAME_CONTROL_EVENT.CHANGE_SNAKE_DIRECTION, x, y);
  }

  private setupSnakeMoveListener() {
    const { snake, gameBoard } = this;

    if (!snake || !gameBoard) return;

    snake.node.on(SNAKE_EVENT.MOVE, this.moveSnake, this);
  }

  private moveSnake(direction: math.Vec2) {
    const { snake, gameBoard } = this;

    if (!snake || !gameBoard) return;

    const { x, y } = snake.getHead().index;
    const nextIndex = v2(x + direction.x, y + direction.y);
    const tile = gameBoard.getTileIfSafe(nextIndex.x, nextIndex.y);

    snake.progressSwallow();

    const eatFruit = gameBoard.eatFruit(nextIndex.x, nextIndex.y);
    if (eatFruit) {
      this.eatFruit();
    }

    if (tile && tile.node) {
      snake.moveHeadTo(nextIndex, tile.node.position);
    } else {
      this.gameOver();
    }

    if (snake.isCannibal()) {
      this.gameOver();
    }

    if (eatFruit) {
      this.gameBoard?.spawnRandomFruit(snake);
    }
  }

  private eatFruit() {
    this.snake?.eatFruit();
    this.node.emit(GAME_CONTROL_EVENT.EAT_FRUIT);

    this.eatSFX?.play();
}

private gameOver() {
    if (!this.isAlive) return;

    this.isAlive = false;

    this.snake?.adjustTextures();
    this.snake?.die();
    this.node.emit(GAME_CONTROL_EVENT.GAME_OVER);

    this.crashSFX?.play();
}
}
