import { _decorator, Component, Node } from "cc";
import { KEYPAD_EVENT } from "../enum/keypad";
import { BaseButton } from "./baseButton";
const { ccclass, property } = _decorator;

@ccclass("KeypadScript")
export class KeypadScript extends Component {
  @property(BaseButton)
  public readonly keypadUP?: BaseButton;

  @property(BaseButton)
  public readonly keypadDown?: BaseButton;

  @property(BaseButton)
  public readonly keypadRight?: BaseButton;

  @property(BaseButton)
  public readonly keypadLeft?: BaseButton;

  start() {
    this.keypadUP?.node.on(Node.EventType.TOUCH_START, () => {
      this.node.emit(KEYPAD_EVENT.PRESS_UP);
    });
    this.keypadDown?.node.on(Node.EventType.TOUCH_START, () => {
      this.node.emit(KEYPAD_EVENT.PRESS_DOWN);
    });
    this.keypadRight?.node.on(Node.EventType.TOUCH_START, () => {
      this.node.emit(KEYPAD_EVENT.PRESS_RIGHT);
    });
    this.keypadLeft?.node.on(Node.EventType.TOUCH_START, () => {
      this.node.emit(KEYPAD_EVENT.PRESS_LEFT);
    });
  }
}
