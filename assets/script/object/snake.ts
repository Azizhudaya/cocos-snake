import {
  _decorator,
  Component,
  Color,
  v2,
  v3,
  instantiate,
  tween,
  macro,
  Vec2,
  Vec3,
  math,
} from "cc";
import { SNAKE_BODY_PART, SNAKE_EVENT } from "../enum/snake";
import {
  SnakeConfig,
  SnakePart,
  SnakeUpdateIntervalConfig,
} from "../interface/snake";
import { SnakeSprite } from "../sprite/snakeSprite";
const { ccclass, property } = _decorator;

@ccclass("Snake")
export class Snake extends Component {
  private readonly baseColor = Color.WHITE;

  private readonly glowColor = new Color(255, 255, 160);

  @property(SnakeSprite)
  public readonly snakeSprite?: SnakeSprite;

  private updateInterval = 1.0;

  private accelerateMultiplier = 1.0;

  private accelerateEvery = 1;

  private minimumInterval = 1.0;

  private eatCounter = 0;

  private parts = new Array<SnakePart>();

  private swallowingParts = new Array<SnakePart>();

  private movementDirection = v2(0, 0);

  public addPart(colIndex: number, rowIndex: number, x: number, y: number) {
    const { snakeSprite } = this;

    if (!snakeSprite) return undefined;

    const sprite = instantiate(snakeSprite.node);
    sprite.setParent(this.node);
    sprite.setPosition(x, y);
    sprite.active = true;

    const part = {
      sprite: sprite.getComponent(SnakeSprite),
      index: v2(colIndex, rowIndex),
      position: v3(x, y, 0),
      rotation: v3(0, 0, 0),
    } as SnakePart;

    this.parts.push(part);

    return part;
  }

  private setPartDirection(
    part: SnakePart,
    directionX: number,
    directionY: number
  ) {
    const { direction } = part;
    const isTurning = this.isPartChangingDirection(
      part,
      directionX,
      directionY
    );

    if (direction) {
      direction.set(directionX, directionY);
    } else {
      part.direction = v2(directionX, directionY);
    }

    if (isTurning) {
      this.rotatePartToMatchDirection(part);
    }
  }

  private getDirectionBetweenParts(partA: SnakePart, partB: SnakePart) {
    return v2(partB.index.x - partA.index.x, partB.index.y - partA.index.y);
  }

  private isPartChangingDirection(
    part: SnakePart,
    directionX: number,
    directionY: number
  ) {
    const { direction } = part;

    if (!direction) return true;

    if (direction.x === directionX && direction.y === directionY) return false;

    return true;
  }

  private getPartRotationByDirection(directionX: number, directionY: number) {
    if (directionY === -1) {
      return v3(0, 0, 0);
    } else if (directionX === 1) {
      return v3(0, 0, -90);
    } else if (directionY === 1) {
      return v3(0, 0, -180);
    } else if (directionX === -1) {
      return v3(0, 0, -270);
    }
  }

  private rotatePartToMatchDirection(part: SnakePart) {
    const { direction, sprite } = part;

    if (!direction) return;

    const nextRotation = this.getPartRotationByDirection(
      direction.x,
      direction.y
    );
    const { rotation: currentRotation } = part || {};
    if (!nextRotation || !currentRotation) return;

    const { x, y, z } = nextRotation;

    const { z: currentZ } = currentRotation;

    /**
     * To prevent sharp turns
     */
    let diffZ = z - currentZ;
    if (diffZ < -180) {
      diffZ = (diffZ + 360) % 360;
    } else if (diffZ > 180) {
      diffZ = (diffZ - 360) % 360;
    }

    part.rotation?.set(x, y, z);
    tween(this.node)
      .to(
        this.updateInterval,
        {},
        {
          onUpdate(_, ratio) {
            if (ratio === undefined) return;
            sprite.node.setRotationFromEuler(x, y, currentZ + diffZ * ratio);
          },
          onComplete() {
            sprite.node.setRotationFromEuler(x, y, z);
          },
        }
      )
      .start();
  }

  public adjustTextures() {
    this.parts.reduce((previousPart, part) => {
      this.adjustPartTexture(previousPart, part);
      return part;
    }, undefined as unknown as SnakePart);
  }

  public adjustPartTexture(previousPart: SnakePart, part: SnakePart) {
    const { sprite } = part;
    if (previousPart) {
      const { x, y } = this.getDirectionBetweenParts(part, previousPart);

      const isTail = part === this.getTail();
      if (isTail) {
        sprite.adjustTexture(SNAKE_BODY_PART.TAIL);
      } else {
        sprite.adjustTexture(SNAKE_BODY_PART.BODY);
      }

      this.setPartDirection(part, x, y);
    } else {
      const { x, y } = this.movementDirection;

      sprite.adjustTexture(SNAKE_BODY_PART.HEAD);

      this.setPartDirection(part, x, y);
    }
  }

  public startMoving() {
    this.updateMoveScheduler();
  }

  private updateMoveScheduler() {
    this.unschedule(this.move);
    this.schedule(this.move, this.updateInterval, macro.REPEAT_FOREVER);
  }

  public move() {
    this.node.emit(SNAKE_EVENT.MOVE, this.movementDirection);
  }

  public die() {
    this.unschedule(this.move);
  }

  public getHead() {
    return this.parts[0];
  }

  public getTail() {
    return this.parts[this.parts.length - 1];
  }

  public getNeck() {
    return this.parts[1];
  }

  private updatePart(part: SnakePart, index: Vec2, position: Vec3) {
    const { x, y } = index;
    const { x: posX, y: posY } = position;

    part.index.set(x, y);
    part.position.set(position);
    tween(part.sprite.node)
      .to(this.updateInterval, {
        position: v3(posX, posY),
      })
      .start();
  }

  private moveParts() {
    this.parts.reduce((previousPart, part) => {
      const { x, y } = part.index;
      const { x: posX, y: posY } = part.position;
      if (previousPart) {
        this.updatePart(part, previousPart.index, previousPart.position);
      }
      return {
        index: v2(x, y),
        position: v3(posX, posY),
      };
    }, undefined as unknown as { index: math.Vec2; position: math.Vec3 });
  }

  public moveHeadTo(index: Vec2, position: Vec3) {
    this.moveParts();
    this.updatePart(this.getHead(), index, position);
    this.adjustTextures();
  }

  private isLegalMove(directionX: number, directionY: number) {
    const head = this.getHead();
    const neck = this.getNeck();

    if (!head || !neck) return true;

    return (
      !(
        head.index.x + directionX === neck.index.x &&
        head.index.y + directionY === neck.index.y
      ) &&
      !(
        this.movementDirection.x === directionX &&
        this.movementDirection.y === directionY
      )
    );
  }

  public changeDirection(directionX: number, directionY: number) {
    if (this.isLegalMove(directionX, directionY)) {
      this.movementDirection.set(directionX, directionY);
      return true;
    }
    return false;
  }

  public getParts() {
    return this.parts;
  }

  public isCannibal() {
    const head = this.getHead();
    return this.getParts().reduce((res, part) => {
      if (part === head) return res;

      if (head.index.x === part.index.x && head.index.y === part.index.y) {
        return true;
      }

      return res;
    }, false);
  }

  private setupInterval(config: SnakeUpdateIntervalConfig) {
    const { initial, accelerateMultiplier, accelerateEvery, minimum } = config;

    this.updateInterval = initial;
    this.accelerateMultiplier = accelerateMultiplier;
    this.accelerateEvery = accelerateEvery;
    this.minimumInterval = minimum;
  }

  public initialize(config: SnakeConfig) {
    this.setupInterval(config.interval);

    const head = this.getHead();
    const neck = this.getNeck();

    if (!head || !neck) return;

    const { x, y } = this.getDirectionBetweenParts(neck, head);
    this.movementDirection.set(x, y);
    this.adjustTextures();
  }

  public eatFruit() {
    this.startSwallow(this.getNeck());
  }

  public startSwallow(part: SnakePart) {
    const { glowColor, baseColor } = this;

    tween(part.sprite.node)
      .to(
        this.updateInterval * 0.5,
        {
          scale: v3(2, 1, 1),
        },
        {
          onStart() {
            part.sprite.setColor(glowColor);
          },
        }
      )
      .to(
        this.updateInterval * 0.5,
        {
          scale: v3(1, 1, 1),
        },
        {
          onComplete() {
            part.sprite.setColor(baseColor);
          },
        }
      )
      .start();
    this.swallowingParts.push(part);
  }

  public progressSwallow() {
    const nextParts = this.swallowingParts.reduce((res, part) => {
      const parts = this.getParts();
      const nextPart = parts[parts.indexOf(part) + 1];

      if (nextPart) {
        res.push(nextPart);
      }

      return res;
    }, new Array<SnakePart>());

    this.swallowingParts = [];

    nextParts.forEach((part) => {
      if (part === this.getTail()) {
        this.incrementEatCounter();
        this.spawnNewTail();
      } else {
        this.startSwallow(part);
      }
    });
  }

  private spawnNewTail() {
    const tail = this.getTail();
    const { index, position, rotation } = tail;

    const part = this.addPart(index.x, index.y, position.x, position.y);

    part?.rotation.set(rotation);
    part?.sprite.node.setRotationFromEuler(rotation);

    part?.sprite.node.setScale(0, 0, 1);
    tween(part?.sprite.node)
      .to(this.updateInterval * 0.7, {
        scale: v3(1.25, 1, 1),
      })
      .to(this.updateInterval * 0.3, {
        scale: v3(1, 1, 1),
      })
      .start();
  }

  private incrementEatCounter() {
    this.eatCounter += 1;
    if (this.eatCounter % this.accelerateEvery === 0) {
      this.updateInterval = Math.max(
        this.updateInterval * this.accelerateMultiplier,
        this.minimumInterval
      );
    }
    this.updateMoveScheduler();
  }

  public getMovementDirection() {
    return this.movementDirection;
  }
}
