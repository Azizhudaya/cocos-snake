import { _decorator, Component } from "cc";
const { ccclass } = _decorator;

@ccclass("IntroStageHeader")
export class IntroStageHeader extends Component {
  public show() {
    this.node.active = true;
  }

  public hide() {
    this.node.active = false;
  }
}
