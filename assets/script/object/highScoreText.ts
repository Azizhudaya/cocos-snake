import { _decorator, Component, Node, CCClass } from 'cc';
import { BaseText } from '../lib/text/baseText';
import { getHighscoreFromLocalStorage } from '../lib/util/localStorage';
const { ccclass, property } = _decorator;

@ccclass("HighScoreTextControl")
export class HighCcoreTextControl extends Component {
  @property(BaseText)
  private highScoreText: BaseText | null

  start() {
    this.highScoreText?.setText(getHighscoreFromLocalStorage().toString());
  }
}