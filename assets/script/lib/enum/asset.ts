export enum ASSET_EXTENSION {
  PNG = ".png",
}

export enum ASSET_TYPE {
  IMAGE = 'image',
  SPRITESHEET = 'spritesheet',
  AUDIO = 'audio',
  FONT = 'font',
}

export enum ASSET_KEY {
  // Temp Sprite
  WHITE_BOX_SPRITE = 'white_box_sprite',

  // Sprite
  LOGO_SPRITE = 'logo_sprite',
  APPLE_SPRITE = 'apple_sprite',
  SOUND_ON_SPRITE = 'sound_on_sprite',
  SOUND_OFF_SPRITE = 'sound_off_sprite',
  TROPHY_SPRITE = 'trophy_sprite', 
  WALL_SPRITE = 'wall_sprite',

  // Spritesheet
  KEYPAD_SPRITESHEET = 'keypad_spritesheet',
  TILE_SPRITESHEET = 'tile_spritesheet',
  SNAKE_SPRITESHEET = 'snake_spritesheet',

  // Soundtrack
  BACKGROUND_SOUNDTRACK = 'background_soundtrack',

  // SFX
  EAT_SFX = 'eat_sfx',
  TURN_SFX = 'turn_sfx',
  CRASH_SFX = 'crash_sfx',
  SILENCE_SFX = 'silence_sfx',

  // Font
  SHOPEE_2021_BOLD = 'shopee_2021_bold',
  SHOPEE_2021_MEDIUM = 'shopee_2021_medium',
}
