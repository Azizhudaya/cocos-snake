import { ASSET_KEY, ASSET_TYPE } from "../enum/asset";
import { AssetConfig } from "../interface/asset";

export function getAssets(): AssetConfig[] {
  const assets = new Array<AssetConfig>();

  // load sprite
  assets.push({
    key: ASSET_KEY.WHITE_BOX_SPRITE,
    type: ASSET_TYPE.IMAGE,
    url: "image/white_box",
  });
  assets.push({
    key: ASSET_KEY.LOGO_SPRITE,
    type: ASSET_TYPE.IMAGE,
    url: "image/logo_shopee_ular",
  });

  // load font
  assets.push({
    key: ASSET_KEY.SHOPEE_2021_BOLD,
    type: ASSET_TYPE.FONT,
    url: "font/Shopee2021-Bold",
  });
  assets.push({
    key: ASSET_KEY.SHOPEE_2021_MEDIUM,
    type: ASSET_TYPE.FONT,
    url: "font/Shopee2021-Medium",
  });

  // load more sprite
  assets.push({
    key: ASSET_KEY.APPLE_SPRITE,
    type: ASSET_TYPE.IMAGE,
    url: "image/sprite_apple",
  });
  assets.push({
    key: ASSET_KEY.SOUND_ON_SPRITE,
    type: ASSET_TYPE.IMAGE,
    url: "image/sprite_sound_on",
  });
  assets.push({
    key: ASSET_KEY.SOUND_OFF_SPRITE,
    type: ASSET_TYPE.IMAGE,
    url: "image/sprite_sound_off",
  });
  assets.push({
    key: ASSET_KEY.TROPHY_SPRITE,
    type: ASSET_TYPE.IMAGE,
    url: "image/sprite_trophy",
  });
  assets.push({
    key: ASSET_KEY.WALL_SPRITE,
    type: ASSET_TYPE.IMAGE,
    url: "image/sprite_wall",
  });

  //load spritesheet
  assets.push({
    key: ASSET_KEY.KEYPAD_SPRITESHEET,
    type: ASSET_TYPE.SPRITESHEET,
    url: "image/keypad",
    config: {
      frameWidth: 124,
      frameHeight: 124,
      paddingX: 20,
      paddingY: 16,
    },
  });
  assets.push({
    key: ASSET_KEY.TILE_SPRITESHEET,
    type: ASSET_TYPE.SPRITESHEET,
    url: "image/sprite_tile",
    config: {
      frameWidth: 48,
      frameHeight: 48,
    },
  });
  assets.push({
    key: ASSET_KEY.SNAKE_SPRITESHEET,
    type: ASSET_TYPE.SPRITESHEET,
    url: "image/spritesheet_round",
    config: {
      frameWidth: 96,
      frameHeight: 96,
      paddingX: 1,
    },
  });

  // load soundtrack
  assets.push({
    key: ASSET_KEY.BACKGROUND_SOUNDTRACK,
    type: ASSET_TYPE.AUDIO,
    url: "audio/bg-music",
  });

  // load sfx
  assets.push({
    key: ASSET_KEY.EAT_SFX,
    type: ASSET_TYPE.AUDIO,
    url: "audio/eat",
  });
  assets.push({
    key: ASSET_KEY.TURN_SFX,
    type: ASSET_TYPE.AUDIO,
    url: "audio/turn",
  });
  assets.push({
    key: ASSET_KEY.CRASH_SFX,
    type: ASSET_TYPE.AUDIO,
    url: "audio/crash",
  });
  assets.push({
    key: ASSET_KEY.SILENCE_SFX,
    type: ASSET_TYPE.AUDIO,
    url: "audio/silence",
  });

  return assets;
}
