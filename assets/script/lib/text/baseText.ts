import {
  _decorator,
  Component,
  Node,
  Color,
  RichText,
  assetManager,
  TTFFont,
} from "cc";
import { getTextWithColor } from "../util/richText";
const { ccclass, property } = _decorator;

@ccclass("BaseText")
export class BaseText extends Component {
  @property(Color)
  public textColor = new Color(255, 255, 255);

  protected richText?: RichText | null;

  constructor(name: string, protected fontKey: string) {
    super(name);
  }

  onLoad() {
    this.richText = this.getComponent(RichText);
    this.reload();
  }

  update() {
    if (this.isFontLoaded()) {
      this.reload();
    }
  }

  protected isFontLoaded() {
    return this.richText?.useSystemFont;
  }

  protected reload() {
    this.setupFont();
    this.reloadTextWithAssignedColor();
  }

  protected setupFont() {
    if (this.richText) {
      this.richText.font = this.getFont();
    }
  }

  protected reloadTextWithAssignedColor() {
    const { string } = this.richText || { string: "" };
    this.setText(string);
  }

  public setText(text: string) {
    const { richText, textColor } = this;
    if (richText) {
      richText.string = getTextWithColor(text, textColor);
    }
  }

  protected getFont() {
    return assetManager.assets.get(this.fontKey) as TTFFont;
  }
}
