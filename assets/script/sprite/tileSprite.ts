import { _decorator, Component, Node } from "cc";
import { ASSET_KEY } from "../lib/enum/asset";
import { BaseTileSprite } from "../lib/sprite/baseTileSprite";
const { ccclass, property } = _decorator;

@ccclass("TileSprite")
export class TileSprite extends BaseTileSprite {
  constructor() {
    super("TileSprite", ASSET_KEY.TILE_SPRITESHEET, 0);
  }

  public adjustTexture(isEven: boolean) {
    if (isEven) {
      this.setFrame(0);
    } else {
      this.setFrame(1);
    }
    this.reload();
  }
}
