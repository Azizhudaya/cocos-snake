import { _decorator, Component, Node } from "cc";
import { SNAKE_BODY_FRAME, SNAKE_BODY_PART } from "../enum/snake";
import { ASSET_KEY } from "../lib/enum/asset";
import { BaseSprite } from "../lib/sprite/baseSprite";
const { ccclass, property } = _decorator;

@ccclass("SnakeSprite")
export class SnakeSprite extends BaseSprite {
  constructor() {
    super("SnakeSprite", ASSET_KEY.SNAKE_SPRITESHEET, 0);
  }

  public adjustTexture(part: SNAKE_BODY_PART) {
    switch (part) {
      case SNAKE_BODY_PART.HEAD: {
        this.setFrame(SNAKE_BODY_FRAME.HEAD);
        break;
      }
      case SNAKE_BODY_PART.TAIL: {
        this.setFrame(SNAKE_BODY_FRAME.TAIL);
        break;
      }
      case SNAKE_BODY_PART.BODY: {
        this.setFrame(SNAKE_BODY_FRAME.BODY);
        break;
      }
      case SNAKE_BODY_PART.BODY_FAT: {
        this.setFrame(SNAKE_BODY_FRAME.BODY_FAT);
        break;
      }
      default: {
        break;
      }
    }
    this.reload()
  }
}
