import { _decorator } from "cc";
import { ASSET_KEY } from "../lib/enum/asset";
import { BaseSprite } from "../lib/sprite/baseSprite";
const { ccclass } = _decorator;

@ccclass("KeypadUpSprite")
export class KeypadUpSprite extends BaseSprite {
  constructor() {
    super("KeypadUpSprite", ASSET_KEY.KEYPAD_SPRITESHEET, 1);
  }
}
