import { _decorator, Component, Node } from "cc";
import { ASSET_KEY } from "../lib/enum/asset";
import { BaseTileSprite } from "../lib/sprite/baseTileSprite";
const { ccclass } = _decorator;

@ccclass("WallSprite")
export class WallSprite extends BaseTileSprite {
  constructor() {
    super("WallSprite", ASSET_KEY.WALL_SPRITE);
  }
}
