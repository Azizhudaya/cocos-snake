import { _decorator } from "cc";
import { ASSET_KEY } from "../lib/enum/asset";
import { BaseText } from "../lib/text/baseText";
const { ccclass, property } = _decorator;

@ccclass("TextRegular")
export class TextRegular extends BaseText {
  constructor() {
    super("Shopee2021Medium", ASSET_KEY.SHOPEE_2021_MEDIUM);
  }
}
