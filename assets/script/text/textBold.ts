import { _decorator } from "cc";
import { ASSET_KEY } from "../lib/enum/asset";
import { BaseText } from "../lib/text/baseText";
const { ccclass, property } = _decorator;

@ccclass("TextBold")
export class TextBold extends BaseText {
  constructor() {
    super("Shopee2021Bold", ASSET_KEY.SHOPEE_2021_BOLD);
  }
}
