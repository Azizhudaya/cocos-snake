import { _decorator, Component, Node } from "cc";
import { PRELOAD_CONTROL_EVENT } from "../enum/preloadControl";
const { ccclass, property } = _decorator;

@ccclass("PreloadControl")
export class PreloadControl extends Component {
  @property(Node)
  private contohNode: Node | null;

  public registerTouchEvent() {
    this.contohNode?.on(Node.EventType.TOUCH_END, () => {
        this.node.emit(PRELOAD_CONTROL_EVENT.TOUCH_END);
      }
    );
  }
}
