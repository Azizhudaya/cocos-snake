import { _decorator, Component, Node } from "cc";
import { BaseAudio } from "../lib/audio/baseAudio";
import { ASSET_KEY } from "../lib/enum/asset";
const { ccclass, property } = _decorator;

@ccclass("TurnSFX")
export class TurnSFX extends BaseAudio {
  constructor() {
    super("TurnSFX", ASSET_KEY.TURN_SFX, false, 0.4);
  }
}
