import { _decorator, Component, Node } from 'cc';
import { BaseAudio } from '../lib/audio/baseAudio';
import { ASSET_KEY } from '../lib/enum/asset';
const { ccclass, property } = _decorator;

@ccclass('CrashSFX')
export class CrashSFX extends BaseAudio {  
  constructor() {
    super('CrashSFX', ASSET_KEY.CRASH_SFX, false, 0.6)
  }
}

