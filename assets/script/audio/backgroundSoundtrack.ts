import { _decorator } from "cc";
import { BaseAudio } from "../lib/audio/baseAudio";
import { ASSET_KEY } from "../lib/enum/asset";
const { ccclass } = _decorator;

@ccclass("BackgroundSoundtrack")
export class BackgroundSoundtrack extends BaseAudio {
  constructor() {
    super("BackgroundSoundtrack", ASSET_KEY.BACKGROUND_SOUNDTRACK, true, 0.5);
  }
}
